#!/usr/bin/env python3

import os
import subprocess
#from xdg import IconTheme
from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen
from libqtile.lazy import lazy

HOME = os.path.expanduser("~")
BIN_HOME = f"{HOME}/.local/bin"
CONFIG_HOME = f"{HOME}/.config"

mod = "mod4"
browser = "librewolf"
terminal = "st"

@lazy.window.function
def float_to_front(window):
    if window.floating:
        window.cmd_bring_to_front()
    return None

@lazy.window.function
def float_resize(window, width = 0, height = 0):
    w = window.width
    h = window.height
    if window.floating:
        window.cmd_set_size_floating(w + width, h + height)
    return None

keys = [
    # Move through windows
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "Left", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),
    Key([mod], "Right", lazy.layout.right()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "Down", lazy.layout.down()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "Up", lazy.layout.up()),

    # Alt + tab
    # Key([mod], "space", lazy.layout.next()), # Default
    Key(["mod1"], "Tab", lazy.group.next_window(), float_to_front()),
    Key(["mod1", "shift"], "Tab", lazy.group.prev_window(), float_to_front()),

    # Move windows to other position
    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "Left", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),
    Key([mod, "shift"], "Right", lazy.layout.shuffle_right()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up()),

    # Grow size
    Key([mod, "control"], "h", lazy.layout.grow_left()),
    Key([mod, "control"], "Left", lazy.layout.grow_left()),
    Key([mod, "control"], "l", lazy.layout.grow_right()),
    Key([mod, "control"], "Right", lazy.layout.grow_right()),
    Key([mod, "control"], "j", lazy.layout.grow_down()),
    Key([mod, "control"], "Down", lazy.layout.grow_down()),
    Key([mod, "control"], "k", lazy.layout.grow_up()),
    Key([mod, "control"], "Up", lazy.layout.grow_up()),
    Key([mod, "control"], "n", lazy.layout.normalize()), # Reset size

    Key([mod, "shift"], "Return", lazy.layout.toggle_split()),

    # Run terminal
    Key([mod], "Return", lazy.spawn(terminal)), # Default

    # Change layout
    Key([mod], "Tab", lazy.next_layout()),

    # Fullscreen
    Key([mod], "f", lazy.window.toggle_fullscreen()),

    # Floating
    Key([mod, "shift"], "f", lazy.window.toggle_floating()),

    # Kill windows
    Key([mod], "w", lazy.window.kill()),

    # Reload Qtile config
    Key([mod, "control"], "Home", lazy.reload_config()),

    # Shutdown Qtile
    Key([mod, "control"], "End", lazy.shutdown()),

    # Commands
    Key(["control", "shift"], "Escape", lazy.spawn("st -e htop")),
    Key([mod], "Comma", lazy.spawn("rofimoji -a clipboard")),
    Key([mod], "Period", lazy.spawn("rofimoji -a copy")),
    Key([mod, "control"], "q", lazy.spawn(f"{BIN_HOME}/window_opts kill")),
    Key([mod], "q", lazy.spawn(f"{BIN_HOME}/window_opts quit")),
    Key([mod], "r", lazy.spawn("rofi -show run")),

    # Screenshot
    KeyChord([], "Print", [
        Key([], "f",
            lazy.spawn(f"{BIN_HOME}/screenshot_opts fs")),
        Key([], "r",
            lazy.spawn(f"{BIN_HOME}/screenshot_opts r")),
        Key([], "w",
            lazy.spawn(f"{BIN_HOME}/screenshot_opts w"))
    ]),
    KeyChord(["control"], "Print", [
        Key([], "f",
            lazy.spawn(f"{BIN_HOME}/screenshot_opts fs_save")),
        Key([], "r",
            lazy.spawn(f"{BIN_HOME}/screenshot_opts r_save")),
        Key([], "w",
            lazy.spawn(f"{BIN_HOME}/screenshot_opts w_save"))
    ]),

    # XF86
    Key(["control"], "XF86AudioMute", lazy.spawn(f"{BIN_HOME}/pulsemixer_opts default_volume")),
    Key(["control"], "XF86AudioPlay", lazy.spawn("mpc stop")),
    Key([], "XF86HomePage", lazy.spawn(browser)),
    Key([], "XF86AudioLowerVolume", lazy.spawn(f"{BIN_HOME}/pulsemixer_opts down_volume")),
    Key([], "XF86AudioMute", lazy.spawn(f"{BIN_HOME}/pulsemixer_opts toggle_mute")),
    Key([], "XF86AudioNext", lazy.spawn("mpc next")),
    Key([], "XF86AudioPlay", lazy.spawn("mpc toggle")),
    Key([], "XF86AudioPrev", lazy.spawn("mpc prev")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn(f"{BIN_HOME}/pulsemixer_opts up_volume")),
    Key([], "XF86Search", lazy.spawn("rofi -show drun -show-icons")),
]

groups = [
    Group(
        name = "1",
        matches = [
            Match(
                wm_class = ["LibreWolf"])],
        layout = "columns",
        label = "MAIN"),
    Group(
        name = "2",
        matches = [
            Match(
                wm_class = ["st-256color"])],
        layout = "columns",
        label = "DEV"),
    Group(
        name = "3",
        matches = [
            Match(wm_class = [
                "Tibia",
                "hl_linux"])],
        layout = "max",
        label = "GAMES"),
    Group(
        name = "4",
        matches = [
            Match(wm_class = ["cmus"])],
        layout = "max",
        label = "MEDIA"),
    Group(
        name = "5",
        matches = [
            Match(title = ["weechat"],
                  wm_class = ["weechat"])],
        layout = "max",
        label = "CHAT"),
    Group(
        name = "6",
          matches = [
              Match(
                  wm_class = [
                      "TelegramDesktop",
                      "whatsdesk",
                      "discord"])],
          layout = "verticaltile",
          label = "SOCIAL"),
    # Group(name = "5", matches = [], layout = ""),
    # Group(name = "6", matches = [], layout = ""),
    # Group(name = "7", matches = [], layout = ""),
    # Group(name = "8", matches = [], layout = ""),
    # Group(name = "9", matches = [], layout = ""),
    # Group(name = "0", matches = [], layout = ""),
    ]

#groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend(
        [
            Key([mod], i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            Key([mod, "shift"], i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

colors = (
        # Normal Colors
        "#21222c", # [0] black
        "#ff5555", # [1] red
        "#50fa7b", # [2] green
        "#f1fa8c", # [3] yellow
        "#bd93f9", # [4] blue
        "#ff79c6", # [5] magenta
        "#8be9fd", # [6] cyan
        "#f8f8f2", # [7] white
        
        # Bright Colors
        "#6272a4", # [8] black
        "#ff6e6e", # [9] red
        "#69ff94", # [10] green
        "#ffffa5", # [11] yellow
        "#d6acff", # [12] blue
        "#ff92df", # [13] magenta
        "#a4ffff", # [14] cyan
        "#ffffff", # [15] white
        )

layouts = [
    layout.Columns(
        border_focus = colors[1],
        border_normal = colors[0],
        border_on_single = "True",
        border_width = 2,
        insert_position = 1,
        margin = 2),
    layout.Max(
        border_focus = colors[4],
        border_normal = colors[0],
        border_width = 2,
        margin = 2),
    layout.VerticalTile(
        border_focus = colors[3],
        border_normal = colors[0],
        border_width = 2,
        margin = 2,
        single_border_width = 2,
        single_margin = 2),
    # layout.Bsp(),
    # layout.Floating(
    #     border_focus = colors[2],
    #     border_normal = colors[0],
    #     border_width = 2),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(margin = 2, border_focus = "#ff0000"),
    # layout.Stack(num_stacks=2),
    # layout.Tile(
    #     add_after_last = "True",
    #     border_focus = colors[2],
    #     border_width = 2,
    #     expand = "True",
    #     margin = 2,
    #     max_ratio = 0.85,
    #     min_ratio = 0.20,
    #     ratio = 0.65,
    #     ratio_increment = 0.05,
    #     shift_windows = "True"),
    # layout.TreeTab(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    background = colors[0],
    font = "FiraCode Nerd Font",
    fontsize = 12,
    foreground = colors[15],
    padding = 3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        wallpaper = "~/Pictures/ws_galaxy_1280x1024.jpg",
        wallpaper_mode = "fill",
        top = bar.Bar(
            [
                # widget.CurrentLayout(foreground = colors[5]),
                widget.Sep(
                    foreground = colors[8]),
                widget.GroupBox(
                    active = colors[7],
                    borderwidth = 2,
                    center_aligned = True,
                    disable_drag = True,
                    hide_unused = False,
                    highlight_method = "border",
                    inactive = colors[8],
                    margin = 2,
                    this_current_screen_border = colors[8],
                    this_screen_border = colors[8],
                    toggle = False,
                    urgent_border = colors[1],
                    urgent_text = colors[6],
                    use_mouse_wheel = False),
                widget.Sep(
                    foreground = colors[8]),
                widget.WindowName(
                    foreground = colors[6]),
                widget.Sep(
                    foreground = colors[8]),
                widget.Systray(
                    icon_size = 20),
                widget.Sep(
                    foreground = colors[8]),
#                widget.Mpd2(
#                    idle_format = "{play_status} {idle_message}",
#                    idle_message = "MPD not playing",
#                    play_states = {"pause": "⏸️",
#                                   "play": "▶️",
#                                   "stop": "⏹️"},
#                    scroll = True,
#                    status_format = "{play_status} {artist} - {title}",
#                    width = 100),
#                widget.Sep(
#                    foreground = colors[8]),
                widget.Clock(
                    foreground = colors[6],
                    format = "📅 %Y-%m-%d %a ⏰ %H:%M:%S"),
                widget.Sep(
                    foreground = colors[8]),
            ],
            24,
        ),
    ),
]

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[#CONFIGURATION_FILES
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
        Match(wm_class="nm-connection-editor"),
        Match(wm_class="pavucontrol"),
        Match(wm_class="MEGAsync"),
    ],
    border_focus = colors[2],
    border_normal = colors[0],
    border_width = 2
)
auto_fullscreen = True
focus_on_window_activation = "smart"
follow_mouse_focus = False
reconfigure_screens = True

auto_minimize = True

wl_input_rules = None

x11_drag_polling_rate = 60

@hook.subscribe.startup_once
def start_once():
    subprocess.call([f'{CONFIG_HOME}/qtile/autostart.sh'])

#wmname = "LG3D"
