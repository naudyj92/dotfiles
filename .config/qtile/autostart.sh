#!/bin/sh

# Runit Per User Services
#runsvdir "$HOME/.service" &

# Screen Resolution
#xrandr --output LVDS-1 --off --output VGA-1 --primary --mode 1280x1024 --rate 60 &

# Keyboard Layout
setxkbmap -model pc105 -layout es -variant deadtilde &

# Bell & Screen Power
xset b 100 &
xset s 600 600 &
xset s blank &
xset s noexpose &
xset dpms 600 600 600 &

# Startup Programs
numlockx &
udiskie -ans &
nm-applet &
blueman-applet &
keepassxc &
pasystray &
