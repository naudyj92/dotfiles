local lsp = require("lspconfig")

lsp.ccls.setup{}
lsp.gopls.setup{}
lsp.pyright.setup{}
lsp.texlab.setup{}
