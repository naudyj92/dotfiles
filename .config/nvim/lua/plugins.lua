require("lazy").setup({
    -- LSP
    'neovim/nvim-lspconfig',

    -- Completion
    'hrsh7th/cmp-nvim-lsp',
    'hrsh7th/cmp-buffer',
    'hrsh7th/cmp-path',
    'hrsh7th/cmp-cmdline',
    'hrsh7th/nvim-cmp',

    -- Snippets
    {
        'L3MON4D3/LuaSnip',
        version = "2.*",
        build = "make install_jsregexp"
    },
    'saadparwaiz1/cmp_luasnip',

    'honza/vim-snippets',

    'shaunsingh/nord.nvim'
})
