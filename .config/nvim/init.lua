require("setup-plugins")
require("plugins")
require("lsp")
require("completion")

local o = vim.o

-- vim.g.pyindent_open_paren = "shiftwidth()"
-- vim.g.pyindent_nested_paren = "shiftwidth()"
-- vim.g.pyindent_continue = "shiftwidth()"

o.guicursor = ""

o.nu = true
o.relativenumber = true

o.tabstop = 4
o.softtabstop = -1
o.shiftwidth = 4
o.expandtab = true

o.smartindent = true

o.wrap = true

o.swapfile = false
o.backup = false
o.undodir = os.getenv("HOME") .. "/.vim/undodir"
o.undofile = true

o.hlsearch = false
o.incsearch = true

o.termguicolors = true

o.scrolloff = 10
o.signcolumn = "yes"

o.updatetime = 50

o.colorcolumn = "80"

o.clipboard = "unnamedplus"

-- Set the leader key
vim.g.mapleader = " "

vim.cmd[[colorscheme nord]]
