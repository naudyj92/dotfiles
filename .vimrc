call plug#begin()
Plug 'dracula/vim', {'as': 'dracula'}
Plug 'sheerun/vim-polyglot'
Plug 'LunarWatcher/auto-pairs'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
call plug#end()

colorscheme dracula
filetype plugin indent on
set autoindent
set backspace=indent,eol,start
set cc=80
set clipboard=unnamedplus
set cursorline
set encoding=utf-8
set expandtab
set hidden
set history=1000
set incsearch
set mouse=a
set nobackup
set noswapfile
set nowritebackup
set number relativenumber
set path+=**
set shiftwidth=4
set signcolumn=yes
set smarttab
set softtabstop=4
set tabstop=4
set undodir=~/.vim/undodir
set undofile
set updatetime=300
set wildmenu
set wildoptions=pum
set nowrap
syntax on

" plugins config
" coc-snippets
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#_select_confirm() :
      \ coc#expandableOrJumpable() ? '<C-r>=coc#rpc#request("doKeymap", ["snippets-expand-jump",""])<CR>' :
      \ CheckBackspace() ? '<TAB>' :
      \ coc#refresh()

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

let g:coc_snippet_next = '<Tab>'
let g:coc_snippet_prev = '<S-Tab>'

let g:UltiSnipsExpandTrigger = '<C-t>'
let g:UltiSnipsListSnippets = '<C-s>'
let g:UltiSnipsJumpForwardTrigger = '<C-j>'
let g:UltiSnipsJumpBackwardTrigger = '<C-k>'
