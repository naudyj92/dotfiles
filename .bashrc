#!/bin/bash

#neofetch

# If not running interactively, dont do anything
echo $- | grep -q "i" || return
PS1="[\u@\h \W]\$ "

# Shell Options

shopt -s autocd
shopt -s cdspell
shopt -s cmdhist
shopt -s dotglob
shopt -s histappend
shopt -s expand_aliases
shopt -s checkwinsize

# Environment Varibles

[ -e "$HOME/.cargo/env" ] && . "$HOME/.cargo/env"

# Aliases

alias bat="bat -p --color always"
[ -e "/usr/share/git/completion/git-completion.bash" ] \
    && . "/usr/share/git/completion/git-completion.bash" \
    && alias cfg='$(which git) --git-dir=$HOME/.dotfiles/ --work-tree=$HOME' \
    && __git_complete cfg __git_main \
    ||  alias cfg='$(which git) --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias cp="cp -i"
alias ls="ls -T 0"
alias lsd="lsd -h --color always"
alias mst="mpc searchplay title $1"
alias mv="mv -i"
alias paru="paru --bottomup"
alias rm="rm -i"
alias sudo="doas"
alias usv="SVDIR=~/.service sv"
alias vim="nvim"

sproc() {
    ps -C "$1" -o pid=
}

shost() {
    host=$(grep -i "$1" ".local/share/ips/hosts.toml" -A 3)
    [ -n "$host" ] && echo "$host" || echo "Not found."
}
