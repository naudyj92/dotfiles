#!/usr/bin/env python3

import glob
import os
import shutil
import subprocess
import time
import tomli
import tomlkit

CURRENT_YEAR = time.strftime("%Y")
CURRENT_MONTH = time.strftime("%B").lower()
CURRENT_DAY = time.strftime("%d")
HOME = os.path.expanduser("~")
LOGS_FOLDER = f"{HOME}/.local/share/weechat/logs"
HOSTS_FOLDER = f"{HOME}/.local/share/ips"

if subprocess.run(["ps", "-C", "weechat", "-o", "pid="], stdout=subprocess.DEVNULL).returncode != 0 and subprocess.run(["ps", "-C", "weechat-headless", "-o", "pid="], stdout=subprocess.DEVNULL).returncode != 0:
    exit()

data = list()
for file in glob.glob(f"{HOSTS_FOLDER}/*.csv"):
    with open(file, "r") as fo:
        data = data + fo.readlines()
    os.remove(file)

hosts = dict()
with open(f"{HOSTS_FOLDER}/hosts.toml", "rb") as fo:
    hosts = tomli.load(fo)

for h in hosts.keys():
    for k in hosts[h].keys():
        hosts[h][k] = set(hosts[h][k])

for info in data:
    nick, user, host, channel = info.split()[0].split(",")
    if not host in hosts:
        hosts[host] = {
                "nicks": {nick},
                "users": {user},
                "channels": {channel}
                }
    else:
        hosts[host]["nicks"].add(nick)
        hosts[host]["users"].add(user)
        hosts[host]["channels"].add(channel)

for h in hosts.keys():
    for k in hosts[h].keys():
        hosts[h][k] = list(hosts[h][k])

with open(f"{HOSTS_FOLDER}/hosts.toml", "w+") as fo:
    tomlkit.dump(hosts, fo)

if time.strftime("%X") == "00:00:00":
    for year in os.listdir(LOGS_FOLDER):
        if year != CURRENT_YEAR:
            shutil.rmtree(f"{LOGS_FOLDER}/{year}")
    
    for month in os.listdir(f"{LOGS_FOLDER}/{CURRENT_YEAR}"):
        if month != CURRENT_MONTH:
            shutil.rmtree(f"{LOGS_FOLDER}/{CURRENT_YEAR}/{month}")
    
    for _ in os.listdir(f"{LOGS_FOLDER}/{CURRENT_YEAR}/{CURRENT_MONTH}/irc/"):
        if not "#" in _ and not "server.chatzona" in _:
            shutil.rmtree(f"{LOGS_FOLDER}/{CURRENT_YEAR}/{CURRENT_MONTH}/irc/{_}")
    
    for _ in glob.glob(f"{LOGS_FOLDER}/{CURRENT_YEAR}/{CURRENT_MONTH}/irc/*/*"):
        if not time.strftime("%d") in _:
            os.remove(_)
