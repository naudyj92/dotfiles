#!/usr/bin/env python3
# -*- coding: utf-8 -*- #

import os
import time
import weechat

HOME = os.path.expanduser("~")
CURRENT_DATE = time.strftime("%y-%m-%d")
LOCAL_HOME = f"{HOME}/.local"
DATA_FILE = f"{LOCAL_HOME}/share/ips/{CURRENT_DATE}.csv"

SCRIPT_NAME = "save_data"
SCRIPT_AUTHOR = "CatJester"
SCRIPT_VERSION = "230406"
SCRIPT_LICENSE = "GPL3"
SCRIPT_DESC = "Script to save hosts, nicks, users and channels"

weechat.register(SCRIPT_NAME, SCRIPT_AUTHOR, SCRIPT_VERSION, SCRIPT_LICENSE, SCRIPT_DESC, "", "")

def join_cb(data, signal, signal_data):
    nick = signal_data.split("!")[0][1:]
    user = signal_data.split("@")[0].split("!")[1]
    host = signal_data.split()[0].split("@")[1]
    channel = signal_data.split()[2]
    if channel[0] == ":":
        channel = channel[1:]
        
    if not os.path.isfile(DATA_FILE):
        with open(DATA_FILE, "w+") as fo:
            fo.write(f"{nick},{user},{host},{channel}\n")
    else:
        with open(DATA_FILE, "a+") as fo:
            fo.write(f"{nick},{user},{host},{channel}\n")

    return weechat.WEECHAT_RC_OK

join_hook = weechat.hook_signal("*,irc_in2_join", "join_cb", "")
