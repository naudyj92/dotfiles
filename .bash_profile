# .bash_profile

# Environment variables

export BROWSER="firefox"
export EDITOR="nvim"
export PATH="$PATH"
export TERMINAL="st"

[ -d "$HOME/.bin" ] && PATH="$PATH:$HOME/.bin"
[ -d "$HOME/.local/bin" ] && PATH="$PATH:$HOME/.local/bin"
[ -d "$HOME/.local/share/gem/ruby/3.0.0/bin" ] && PATH="$PATH:$HOME/.local/share/gem/ruby/3.0.0/bin"

[ -z "$XDG_CONFIG_HOME" ] && export XDG_CONFIG_HOME="$HOME/.config"
[ -z "$XDG_DATA_HOME" ] && export XDG_DATA_HOME="$HOME/.local/share"
[ -z "$XDG_CACHE_HOME" ] && export XDG_CACHE_HOME="$HOME/.cache"

# Get the aliases and functions
[ -f $HOME/.bashrc ] && . $HOME/.bashrc
[ -e "$HOME/.cargo/env" ] && . "$HOME/.cargo/env"
